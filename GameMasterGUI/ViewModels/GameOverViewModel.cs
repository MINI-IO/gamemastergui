﻿using System;
using System.Collections.Generic;
using System.Text;
using GameMaster.Model;

namespace GameMaster.GUI.ViewModels
{
    public class GameOverViewModel
    {
        public Team? Winners { get; set; }
        public string GameTime { get; set; }
        public int BlueScore { get; set; }
        public int RedScore { get; set; }
        public int NumberOfGoals { get; set; }
    }


}
