﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.GUI.ViewModels
{
    public class ConfigurationViewModel
    {
        public string CsIP { get; set; }
        public int CsPort { get; set; }
        public int MovePenaltyTime { get; set; }
        public int InformationExchangePenaltyTime { get; set; }
        public int DiscoverPenaltyTime { get; set; }
        public int PutPenaltyTime { get; set; }
        public int CheckShamPenaltyTime { get; set; }
        public int DestroyPiecePenaltyTime { get; set; }
        public int BoardHeight { get; set; }
        public int BoardWidth { get; set; }
        public int GoalAreaHeight { get; set; }
        public int TeamPlayersNumber { get; set; }
        public int NumberOfGoals { get; set; }
        public int NumberOfPieces { get; set; }
        public double ShamPieceProbability { get; set; }
      
    }
}
