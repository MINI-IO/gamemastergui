﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.GUI.ViewModels
{
    public class MapViewModel
    {
        public int BoardWidth { get; set; }
        public int BoardHeight { get; set; }
        public FieldType[,] Fields { get; set; }
    }
}
