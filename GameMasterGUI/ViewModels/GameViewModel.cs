﻿using System;
using System.Collections.Generic;
using System.Text;
using GameMaster.Model.Fields;
using GameMaster.Model.Game;
using GameMaster.Model;
using System.Linq;

namespace GameMaster.GUI.ViewModels
{
    public class GameViewModel
    {
        private readonly GameInstance GameInstance;

        public GameViewModel(GameInstance gameInstance)
        {
            this.GameInstance = gameInstance;
        }

        public int BlueTeamScore => GameInstance.BlueTeamScore.Score;
        public int RedTeamScore => GameInstance.RedTeamScore.Score;
        public int NumberOfGoals => GameInstance.Configuration.NumberOfGoals;


        public int BoardWidth => GameInstance.Board.Width;
        public int BoardHeight => GameInstance.Board.Height;
        public int GoalAreaHeight => GameInstance.Board.GoalAreaHeight;
        public FieldType[,] Fields
        {
            get
            {
                var board = GameInstance.Board;

                FieldType[,] fieldTypes = new FieldType[BoardHeight, BoardWidth];

                for (int x = 0; x < BoardWidth; ++x)
                {
                    for(int y = 0; y < BoardHeight; ++y)
                    {
                        fieldTypes[BoardHeight - y - 1, x] =
                            board.GetFieldById(new Point(x, y)).CreateDisplayableCell().FieldType.ModelToGui();
                    }
                }

                return fieldTypes;
            }
        }
        public int BlueTeamNumberOfPlayers 
            => GameInstance.Agents.Values.Where(t => t.Team == Model.Team.Blue).Count();
        public int RedTeamNumberOfPlayers
            => GameInstance.Agents.Values.Where(t => t.Team == Model.Team.Red).Count();
    }
}
