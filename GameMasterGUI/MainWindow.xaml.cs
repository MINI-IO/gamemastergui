﻿using GameMaster.GUI.Views;
using GameMaster.Model.Game;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameMaster.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public GameInstance GameInstance { get; set; }
        public IHost Host { get; set; }
        public bool GameStarted { get; set; }
        public MainWindow()
        {
            DataContext = new ConfigurationView
            {
                Main = this
            };
            InitializeComponent();
            Closing += WindowClosing;
            GameStarted = false;
        }

        private void WindowClosing(object sender, CancelEventArgs e)
        {
            if (!(GameInstance is null) && DataContext is GameView && GameStarted)
            {
                GameInstance.End();
            }

            if (!(Host is null))
            {
                Host.Dispose();
            }
        }
    }
}
