﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace GameMaster.GUI.ValidationRules
{
    class PenaltyTimeValidationRule : ValidationRule
    {
        public int MinValue { get; set; }
        public int Precision { get; set; }

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (int.TryParse(value.ToString(), out int i))
            {
                if (i < MinValue) return new ValidationResult(false, $"Please enter value greater than {MinValue}.");
                if (i % Precision != 0) return new ValidationResult(false, $"Please enter value divisible by {Precision}.");
                else return new ValidationResult(true, null);
            }

            return new ValidationResult(false, "Please enter a valid time span value.");
        }
    }
}
