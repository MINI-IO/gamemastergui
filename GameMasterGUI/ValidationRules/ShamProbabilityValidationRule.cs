﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace GameMaster.GUI.ValidationRules
{
    class ShamProbabilityValidationRule : ValidationRule
    {
        private double MaxValue = 1;
        private double MinValue = 0;
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (double.TryParse(value.ToString(), out double i))
            {
                if(i >= MinValue && i <= MaxValue) return new ValidationResult(true, null);
                else return new ValidationResult(false, $"Please enter a value in range [{MinValue},{MaxValue}].");
            }
            return new ValidationResult(false, "Please enter a valid double value.");
        }
    }
}
