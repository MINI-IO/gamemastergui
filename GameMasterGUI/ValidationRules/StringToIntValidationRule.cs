﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace GameMaster.GUI.ValidationRules
{
    public class StringToIntValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {      
            if (int.TryParse(value.ToString(), out int i))
                return new ValidationResult(true, null);

            return new ValidationResult(false, "Please enter a valid integer value.");
        }
    }
}
