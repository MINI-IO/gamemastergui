﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Controls;

namespace GameMaster.GUI.ValidationRules
{
    public class ValueInRangeValidationRule : ValidationRule
    {
        public int MinValue { get; set; }
        public int? MaxValue { get; set; }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            
            if (int.TryParse(value.ToString(), out int i))
            {
                if(MaxValue.HasValue)
                {
                    if (i <= MaxValue.Value && i >= MinValue) return new ValidationResult(true, null);
                    else return new ValidationResult(false, $"Please enter value in range [{MinValue},{MaxValue.Value}].");
                }
                else
                {
                    if(i < MinValue) return new ValidationResult(false, $"Please enter value greater than {MinValue}.");
                    else return new ValidationResult(true, null);

                }
            }
            return new ValidationResult(false, "Please enter a valid integer value.");
        }
    }
}
