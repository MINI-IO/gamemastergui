﻿using GameMaster.GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace GameMaster.GUI.Views
{
    /// <summary>
    /// Logika interakcji dla klasy GameView.xaml
    /// </summary>
    public partial class GameView : UserControl
    {
        private MainWindow main;

        private DispatcherTimer dispatcherTimer;

        private Stopwatch stopwatch;
        public MainWindow Main { get => main; set => main = value; }

        private Model.Game.GameInstance _gameInstance;
        public Model.Game.GameInstance GameInstance
        {
            get => _gameInstance;
            set
            {
                _gameInstance = value;
                this.UpdateMapControl();
            }
        }

        public Task Host { get; set; }

        public GameViewModel GameViewModel { get; set; }

        public GameView()
        {
            InitializeComponent();

            DataContext = GameViewModel;

            InitializeTimers();
        }

        private void InitializeTimers()
        {
            stopwatch = new Stopwatch();

            dispatcherTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(50),
            };
            dispatcherTimer.Tick += DispatcherTimer_Tick;
            dispatcherTimer.Start();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (GameInstance.GameOver)
            {
                GameEnd(null, null);
            }
            TimeSpan elapsedTime = stopwatch.Elapsed;
            TimerControl.Content = elapsedTime.ToString(@"hh\:mm\:ss");
            UpdateMapControl();
        }

        private void GamePause(object sender, RoutedEventArgs e)
        {
            GameInstance.Pause();
            stopwatch.Stop();
            dispatcherTimer.Stop();
            Overlay.Visibility = Visibility.Visible;
        }

        private void GameContinue(object sender, RoutedEventArgs e)
        {
            GameInstance.Resume();         
            stopwatch.Start();
            dispatcherTimer.Start();
            Overlay.Visibility = Visibility.Collapsed;
            UpdateMapControl();
        }

        private void GameEnd(object sender, RoutedEventArgs e)
        {
            var endGame = new GameViewModel(GameInstance);
            GameInstance.End();
            TimeSpan totalTime = stopwatch.Elapsed;
            stopwatch.Stop();
            dispatcherTimer.Stop();
            main.DataContext = new GameOverView(endGame, totalTime);
        }
        private void GameStart(object sender, RoutedEventArgs e)
        {
            GameInstance.Start();
            StarButton.Visibility = Visibility.Collapsed;
            stopwatch.Start();
            dispatcherTimer.Start();
            UpdateMapControl();
            Main.GameStarted = true;
        }

        public void UpdateMapControl()
        {
            GameViewModel = new GameViewModel(GameInstance);
            DataContext = GameViewModel;

            var Playground = new MapControl(GameViewModel.BoardHeight, GameViewModel.BoardWidth, GameViewModel.Fields);

            SecondaryGrid.Children.Clear();
            SecondaryGrid.Children.Add(Playground);
            Grid.SetRow(Playground, 0);
            Grid.SetColumn(Playground, 0);
        }
    }
}
