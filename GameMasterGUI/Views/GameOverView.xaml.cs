﻿using GameMaster.GUI.ViewModels;
using GameMaster.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GameMaster.GUI.Views
{
    /// <summary>
    /// Logika interakcji dla klasy GameOverView.xaml
    /// </summary>
    public partial class GameOverView : UserControl
    {
        public GameOverView()
        {
            InitializeComponent();
        }

        public GameOverView(GameViewModel game, TimeSpan totalTime) : this()
        {
            GameOverViewModel gameOver = new GameOverViewModel
            {              
                RedScore = game.RedTeamScore,
                BlueScore = game.BlueTeamScore,
                NumberOfGoals = game.NumberOfGoals,
                GameTime = totalTime.ToString(@"hh\:mm\:ss")
            };

            gameOver.Winners = GetWinners(game);

            DataContext = gameOver;

            var Playground = new MapControl(game.BoardHeight, game.BoardWidth, game.Fields);

            SecondaryGrid.Children.Clear();
            SecondaryGrid.Children.Add(Playground);
            Grid.SetRow(Playground, 0);
            Grid.SetColumn(Playground, 0);

            PrintWinners(gameOver);

        }
        private Team? GetWinners(GameViewModel game)
        {
            if (game.RedTeamScore == game.NumberOfGoals) return Team.Red;
            else if (game.BlueTeamScore == game.NumberOfGoals) return Team.Blue;
            else if (game.RedTeamScore == game.BlueTeamScore) return null;
            else return game.RedTeamScore > game.BlueTeamScore ? Team.Red : Team.Blue;
        }

        private void PrintWinners(GameOverViewModel gameOver)
        {
            if(gameOver.Winners.HasValue)
            {
                if(gameOver.Winners.Value == Team.Blue)
                {
                    WinnersTitle.Content = "Blue team wins!";
                    WinnersTitle.Foreground = (Brush)FindResource("BluePlayerBrush");
                }
                else
                {
                    WinnersTitle.Content = "Red team wins!";
                    WinnersTitle.Foreground = (Brush)FindResource("RedPlayerBrush");
                }
            }
            else
            {
                WinnersTitle.Content = "Draw!";
            }
        }
    }
}
