﻿﻿using GameMaster.Communication;
using GameMaster.Communication.DTOs;
using GameMaster.DataAccess;
using GameMaster.GUI.ViewModels;
using IoCommunication;
using Microsoft.Extensions.Hosting;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace GameMaster.GUI.Views
{
    /// <summary>
    /// Logika interakcji dla klasy ConfigurationView.xaml
    /// </summary>
    public partial class ConfigurationView : UserControl
    {
        private MainWindow main;

        public MainWindow Main { get => main; set => main = value; }

        public ConfigurationView()
        {
            InitializeComponent();
            DataContext = new GameMaster.Model.Game.ConfigurationViewModel()
            {
                CsIP = "127.0.0.1",
                CsPort = 8000,
                MovePenaltyTime = 100,
                InformationExchangePenaltyTime = 100,
                DiscoverPenaltyTime = 100,
                PutPenaltyTime = 100,
                CheckShamPenaltyTime = 100,
                DestroyPiecePenaltyTime = 100,
                BoardHeight = 12,
                BoardWidth = 7,
                GoalAreaHeight = 3,
                TeamPlayersNumber = 4,
                NumberOfGoals = 4,
                NumberOfPieces = 2,
                ShamPieceProbability = 0.5
            };
        }

        private void ConfigurationNext(object sender, RoutedEventArgs e)
        {
            if (!IsValid(this)) return;

            var repo = new SingleGameRepository();

            var controller = new GameMaster.Communication.Controllers.GMController(repo);

            var messageHandler = new BasicMessageHandler<MessageWrapperDTO>(
                controller
                );

            var communicationService = new SenderService(messageHandler, repo);


            GameMaster.Model.Game.ConfigurationViewModel config = (GameMaster.Model.Game.ConfigurationViewModel)DataContext;

            var game = Model.Game.GameFactory.CreateGame(config, communicationService);

            repo.Register(game);

            var host = Host.CreateDefaultBuilder().CreateTcpClient(messageHandler).Build();

            this.Main.DataContext = new GameView()
            {
                GameInstance = game,
                GameViewModel = new GameViewModel(game),
                Main = this.Main,
                Host = host.RunAsync()
            };
            this.Main.GameInstance = game;
            this.Main.Host = host;

        }

        //https://stackoverflow.com/questions/127477/detecting-wpf-validation-errors
        public static bool IsValid(DependencyObject parent)
        {
            // Validate all the bindings on the parent
            bool valid = true;
            LocalValueEnumerator localValues = parent.GetLocalValueEnumerator();
            while (localValues.MoveNext())
            {
                LocalValueEntry entry = localValues.Current;
                if (BindingOperations.IsDataBound(parent, entry.Property))
                {
                    Binding binding = BindingOperations.GetBinding(parent, entry.Property);
                    foreach (ValidationRule rule in binding.ValidationRules)
                    {
                        ValidationResult result = rule.Validate(parent.GetValue(entry.Property), null);
                        if (!result.IsValid)
                        {
                            BindingExpression expression = BindingOperations.GetBindingExpression(parent, entry.Property);
                            Validation.MarkInvalid(expression, new ValidationError(rule, expression, result.ErrorContent, null));
                            valid = false;
                        }
                    }
                }
            }

            // Validate all the bindings on the children
            for (int i = 0; i != VisualTreeHelper.GetChildrenCount(parent); ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (!IsValid(child)) { valid = false; }
            }

            return valid;
        }

        // TO DO:
        // - walidacja adresu IP
        // - walidacja wartości wzajmnie zależnych
        // - maksy w wartościach
    }
}
