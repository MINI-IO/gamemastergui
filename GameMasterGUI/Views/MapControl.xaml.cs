﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GameMaster.GUI.ViewModels;
using GameMaster.GUI;

namespace GameMaster.GUI.Views
{
    /// <summary>
    /// Interaction logic for MapControl.xaml
    /// </summary>
    public partial class MapControl : UserControl
    {
        public MapControl(int a, int b, FieldType[,] ft)
        {
            BoardHeight = a;
            BoardWidth = b;
            Fields = ft;
            InitializeComponent();
            SetRenderResources();
            RenderMap(Fields);
        }

        public int BoardHeight { get; set; }
        public int BoardWidth { get; set; }
        public FieldType[,] Fields { get; set; }

        private IDictionary<FieldType, (Brush brush, string text)> renderResources;

        private void RenderMap(FieldType[,] fields)
        {
            MapGrid = new System.Windows.Controls.Primitives.UniformGrid
            {
                Rows = BoardHeight,
                Columns = BoardWidth
            };
            for (int i = 0; i < BoardHeight; ++i)
            {
                for (int j = 0; j < BoardWidth; ++j)
                {
                    Label label = new Label
                    {
                        Background = renderResources[fields[i, j]].brush,
                        Content = renderResources[fields[i, j]].text
                    };
                    MapGrid.Children.Add(label);
                }
            }
            MainGrid.Children.Add(MapGrid);
            //Grid.SetRow(MapGrid, 0);
            //Grid.SetColumn(MapGrid, 0);
        }

        private void SetRenderResources()
        {
            renderResources = new Dictionary<FieldType, (Brush brush, string text)>
            {
                { FieldType.EmptyRedArea, ((Brush)FindResource("EmptyRedBrush"), "") },
                { FieldType.EmptyBlueArea, ((Brush)FindResource("EmptyBlueBrush"), "") },
                { FieldType.EmptyTaskArea, ((Brush)FindResource("EmptyTaskBrush"), "") },
                { FieldType.RedPlayer, ((Brush)FindResource("RedPlayerBrush"), "R") },
                { FieldType.BluePlayer, ((Brush)FindResource("BluePlayerBrush"), "B") },
                { FieldType.RedPlayerWithPiece, ((Brush)FindResource("RedPlayerBrush"), "RP") },
                { FieldType.BluePlayerWithPiece, ((Brush)FindResource("BluePlayerBrush"), "BP") },
                { FieldType.Piece, ((Brush)FindResource("PieceBrush"), "P") },
                { FieldType.DiscoveredGoal, ((Brush)FindResource("DiscoveredGoalBrush"), "DG") },
                { FieldType.UndiscoveredGoal, ((Brush)FindResource("UndiscoveredGoalBrush"), "G") },
                { FieldType.DiscoveredNonGoal, ((Brush)FindResource("DiscoveredNonGoalBrush"), "N") }
            };
        }


    }
}
