﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GameMaster.GUI
{
    public enum FieldType
    {
        EmptyRedArea,
        EmptyBlueArea,
        EmptyTaskArea,
        RedPlayer,
        BluePlayer,
        RedPlayerWithPiece,
        BluePlayerWithPiece,
        Piece,
        DiscoveredGoal,
        UndiscoveredGoal,
        DiscoveredNonGoal
    }

    public static class Helper
    {
        public static FieldType ModelToGui(this Model.FieldType ft)
            => ft switch
            {
                Model.FieldType.EmptyRedArea => FieldType.EmptyRedArea,
                Model.FieldType.EmptyBlueArea => FieldType.EmptyBlueArea,
                Model.FieldType.EmptyTaskArea => FieldType.EmptyTaskArea,
                Model.FieldType.RedPlayer => FieldType.RedPlayer,
                Model.FieldType.BluePlayer => FieldType.BluePlayer,
                Model.FieldType.RedPlayerWithPiece => FieldType.RedPlayerWithPiece,
                Model.FieldType.BluePlayerWithPiece => FieldType.BluePlayerWithPiece,
                Model.FieldType.Piece => FieldType.Piece,
                Model.FieldType.DiscoveredGoal => FieldType.DiscoveredGoal,
                Model.FieldType.UndiscoveredGoal => FieldType.UndiscoveredGoal,
                Model.FieldType.DiscoveredNonGoal => FieldType.DiscoveredNonGoal,
                Model.FieldType.UndiscoveredNonGoal => FieldType.EmptyTaskArea,
                _ => FieldType.EmptyTaskArea
            };
    }
}
